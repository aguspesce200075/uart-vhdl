----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:47:29 03/17/2024 
-- Design Name: 
-- Module Name:    uart_module - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity uart_module is
	generic(TRAMA: integer:=8);
	port(clk_in :in std_logic;
		  data :in std_logic;
		  reset : in std_logic;
		  salida :out std_logic_vector (TRAMA-1 downto 0)
	);
end uart_module;

architecture Behavioral of uart_module is
	type state is (no_data, r_start,r_bits,r_stop);
	signal next_state   		:state;
	signal r_registro    		:std_logic_vector(TRAMA-1 downto 0);
	signal r_buffer     			:std_logic_vector(TRAMA-1 downto 0);
	signal contador_bits 		:std_logic_vector(2 downto 0);
	signal contador_clock		:std_logic_vector(17 downto 0);
	constant baud_rate         :integer:=9600;
	constant cantidad_pulsos   :integer:=(2000000/baud_rate) -1;

begin
	rx: process (clk_in) begin
		if clk_in'event and clk_in='1' then
			if reset='1' then
				next_state<=no_data;
			else
				case next_state is
				when no_data =>
					contador_bits <=(others =>'0');
					contador_clock <=(others=>'0');
					if data='0' then
						next_state<=r_start;
					end if;
				when	r_start =>
					if(cantidad_pulsos<contador_clock/2) then
						contador_clock <= contador_clock + 1;
					else
						contador_clock<=(others=>'0');
						if data ='1' then
							next_state<=no_data;
						else
							next_state<=r_bits;
						end if;
					end if;
				end case;
			end if;
		end if;
	end process;

end Behavioral;

